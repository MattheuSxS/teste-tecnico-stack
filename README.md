## Desafio de Data Engineering

O objetivo do projeto é extrair os dados do OPENDATASUS via API, fazer a limpeza e transformação dos dados e depois injetar os dados limpos e tratados nas seguintes locais

    BigQuery
    BigTable
    Cloud Sql ( Mysql )
    Cloud Storage
    FileSystem ( Local )


## Resumo Geral
Para realização do desafio foi utilizado Python 3.10.9. Para executar o código localmente é necessário fazer o download do projeto dando um `Git clone` e instalar as dependências necessárias para a execução do código. Todos os comandos abaixo são executados no **CMD**, na **pasta raiz do projeto**.
Recomendo ao usuário usar uma das opções abaixo como ferramenta de criação do ambiente:

- Anaconda/Miniconda:

        Conda create –n $(basename $(pwd)) python=3.10.9
        conda activate $(basename $(pwd))

- Pyenv

        python3.10 -m venv app_env
        source app_env/bin/activate


Para instalar as dependencias necessárias para rodar o código use o seguinte comando;

    pip install -r requirements-dev.txt
    ou
    pip3 install -r requirements-dev.txt

Leva alguns minutos para completar toda a instalação e o ambiente está pronto para uso.
Às vezes quando e criado um ambiente virtual o Python pode não reconhecer o diretório e apresentar alguns erros estranhos, então é recomendado usar o comando abaixo na pasta raiz do projeto;

    export PYTHONPATH=$(pwd)

# Montando o ambiente no Google Cloud Platform
Para montar o todo o ambiente no GCP foi usado o terraform para executá-lo é muito simple, basta " stack_challange/stack_challange/infra/terraform" digitar os seguintes comandos abaixo, porém antes é necessário o usuário por o seu ip no arquivo mysql_ips.tfvars.

    terraform init
    terraform validate
    terraform plan -var-file mysql_ips.tfvars -auto-approve
    terraform apply -var-file mysql_ips.tfvars -auto-approve

Obs: Para fazer esta etapa Local ou CI/CD ( gitlab ) é necessário já está credenciado ou ter sua chave ..json


Para poder encontrar a instância do Cloud Mysql é necessário esperar rodar com sucesso o terraform entrar no Google Cloud Platform ir na aba Cloud SQL e pegar o IP público e setar a variável abaixo:

    export GCP_INSTANCE_PUBLIC_IP='<ip dado pelo GCP>'

Obs: Na pasta stack_challange contém o arquivo variable_env.sh, nele contém todas as variáveis do ambiente necessárias para rodar o código python com sucesso!



Com o ambiente criado e dependências instaladas, o usuário pode mandar o seguinte comando na pasta raiz 2rp_challenge/src.

    python main.py
    ou
    Python3 main.py


![alt text](./stack_challange/DOC/stack_challange.png)

## Estrutura de código
```
.
├── stack_challange
│   ├── datasets
│   │   └── *.parquet
│   ├── DOC
│   │   └── stack_challange.jpg
│   ├── flow
│   │   └── dags
│   │       ├── script
│   │       │   └── *.py
│   │       ├── temp_data
│   │       │   └── *.parquet
│   │       └── pipeline-trigger-dev.py
│   ├── infra
│   │   └── terraform
│   │       ├── schemas
│   │       │   └── *.json
│   │       └── *.tf
│   └── src
│       ├── tests
│       │   ├── __init__.py
│       │   └── test_*.py
│       ├── __init__.py
│       ├── .coveragerc
│       ├── gcp_bigquery.py
│       ├── gcp_bigtable.py
│       ├── gcp_bucket.py
│       ├── gcp_mysql.py
│       ├── main.py
│       ├── Makefile
│       ├── modulos.py
│       ├── requirements-dev.txt
│       ├── requirements.txt
│       ├── schema
│       ├── setup.py
│       └── version.py
├── .gitignore
├── .gitlab-ci.yml
├── .python-version
├── Makefile
├── README.md
└── variable_env.sh
```


## Como esses pipelines são executados
O [airflow Scheduler](https://airflow.apache.org/docs/apache-airflow/2.5.1/concepts/scheduler.html). Atualmente, essas agregações são executadas uma vez por dia:

- Every day at 6:30 A.M. (America/Sao_Paulo).


## Testes Automatizados
### TODO --> :construction: in progress :construction:
O projeto tem duas pastas de testes localizadas em;

- stack_challenge/stack_challenge
    - tests
	- validates

- stack_challenge/src
	- tests

- O usuário executa esses testes entrando em um dos dois diretórios citado acima e executando um dos comandos abaixo;
    - make test
    - make coverage

Os testes estão automatizados no CI/CD do Gitlab


## Gitlab CI/CD
O projeto é deployado pelo CI/CD do Gitlab no repositório `https://gitlab.com/MattheuSxS/stack_challenge`. Há três etapas a serem feitas para um deploy ser bem sucedido! São elas;

- Stages:
    - terraform-test --> Faz a validação dos arquivos
    - terraform-deploy --> Faz o deploy da aplicação
    - terraform-destroy --> Destroy a aplicação


Obs: A princípio não há uma forma fácil de pegar o IP Público gerado pela instância do MySql, então após a execução com sucesso do terraform! Ir na aba " Instância SQL" coletar o Ip público disponibilizado e por nas variáveis de ambiente do cloud composer, na aba "variáveis de ambiente"


## Referências

- GitLab:
    - [Documento GitLab](https://docs.gitlab.com)
    - [Comando GitLab Clone](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html)
    - [GitLab CI/CD](https://docs.gitlab.com/ee/ci/ )

- Terraform:
    - [Documento Terraform](https://developer.hashicorp.com/terraform/intro)
    - [Terraform Google](https://registry.terraform.io/providers/hashicorp/google/latest)

- Python:
    - [Documento Python](https://docs.python.org/3.10/)
    - [Documento Pandas](https://pandas.pydata.org/docs/)
    - [Documento Polars](https://pola-rs.github.io/polars/py-polars/html/index.html)
    - [Book Polars](https://pola-rs.github.io/polars-book/user-guide/introduction.html)
    - [Documento PySpark](https://spark.apache.org/docs/latest/api/python/index.html)
    - [Documento Pytest](https://docs.pytest.org/en/7.2.x/contents.html)
    - [Documento Coverage](https://coverage.readthedocs.io/en/6.5.0/)

- Airflow:
    - [Documento Airflow](https://airflow.apache.org/docs/)
    - [Scheduler Airflow](https://airflow.apache.org/docs/apache-airflow/1.10.13/scheduler.html)

- Google Cloud (Google Cloud Platform):
    - [Documento Composer](https://cloud.google.com/composer/docs/run-apache-airflow-dag)
    - [Documento Cloud Storage](https://cloud.google.com/storage/docs?hl=pt-br)
    - [Documento BigQuery](https://cloud.google.com/bigquery/docs?hl=pt-br)
    - [Documento BigTable](https://cloud.google.com/bigtable/docs?hl=pt-br)
    - [Documento Cloud Sql](https://cloud.google.com/sql/docs?hl=pt-br)

- Docker:
    - [Documento Docker](https://docs.docker.com)
    - [Docker Airflow](https://hub.docker.com/r/apache/airflow)

- Makefile
    - [Documento Makefile](https://www.gnu.org/software/make/manual/make.html)
    - [Documento Tutorial](https://makefiletutorial.com)


| Versão Do Documento |        Editor      |    Data    |
|        :---:        |        :---:       |    :---:   |
|        1.0.0        | Matheus Dos Santos | 20/03/2023 |