.PHONY: build-function deploy-airflow destroy-airflow

build-function:
	source ./variable_env.sh

deploy-airflow:
	docker run -d -p 8080:8080 -v "$(PWD)/stack_challange/flow/dags:/opt/airflow/dags/" \
	--env "API_USERNAME=imunizacao_public" \
	--env "API_PASSWORD=qlto5t&7r_@+#Tlstigi" \
	--env "GCP_PROJECT_ID=gcp-data-pipeline-stack" \
	--env "GCP_PROJECT_REGION=us-east1" \
	--env "GCP_BIGQUERY_DATASET_ID=product_analytical" \
	--env "GCP_BIGQUERY_TABLE_ID=sus_analytical" \
	--env "GCP_BUCKET_ID=bkt-filesystem-stack" \
	--env "GCP_BIGTABLE_INSTANCE_ID=stack-instance-bigtable" \
	--env "GCP_BIGTABLE_TABLE_ID=analytical-stack" \
	--env "GCP_INSTANCE_MYSQL=stack-instance-mysql" \
	--env "GCP_DATABASE_MYSQL=stack-database" \
	--env "GCP_TABLE_MYSQL=analytica_stack" \
	--env "GCP_INSTANCE_PUBLIC_IP=< pegar e inserido manualmente depois de criar a instancia MYSQL via TERRAFORM >" \
	--env "TF_VAR_sql_mysql_username=matheus" \
	--env "TF_VAR_sql_mysql_password=87654321a" \
	--entrypoint=/bin/bash \
	--name airflow apache/airflow:2.5.1-python3.10 \
	-c '(airflow db init && \
		airflow users create --username admin --password 87654321a --firstname Matheus --lastname Santos --role Admin --email mattheusxs@gmail.com); \
	airflow webserver & \
	airflow scheduler'


destroy-airflow:
	docker rm -f airflow