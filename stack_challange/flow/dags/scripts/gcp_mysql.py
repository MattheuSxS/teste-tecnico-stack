import polars as pl
import mysql.connector
import logging
import os


logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


class SqlCloudMySql:
    def __init__(self):
        self.table_name = os.environ.get('GCP_TABLE_MYSQL')
        self.config = {
            'user': os.environ.get('TF_VAR_sql_mysql_username'),
            'password': os.environ.get('TF_VAR_sql_mysql_password'),
            'host': os.environ.get('GCP_INSTANCE_PUBLIC_IP'),
            'database': os.environ.get('GCP_DATABASE_MYSQL'),
            'use_pure': True
            }

        self.cnx = mysql.connector.connect(**self.config)
        self.cursor = self.cnx.cursor()


    def create_table(self):

        logging.info(f"check if {self.table_name} table exists")
        self.cursor.execute(
            '''
                CREATE TABLE IF NOT EXISTS analytica_stack (
                    id INT AUTO_INCREMENT PRIMARY KEY,
                    document_id VARCHAR(255),
                    paciente_id VARCHAR(255),
                    paciente_dataNascimento DATE,
                    paciente_idade VARCHAR(255),
                    paciente_enumSexoBiologico VARCHAR(255),
                    paciente_racaCor_valor VARCHAR(255),
                    paciente_endereco_uf VARCHAR(255),
                    vacina_fabricante_nome VARCHAR(255),
                    vacina_lote VARCHAR(255),
                    vacina_dataAplicacao DATETIME,
                    data_importacao_rnds DATETIME
                );
            '''
            )

        logging.info(f"{self.table_name} Table Already Existing/Created!")


    def insert_mysql(self, data_pl: pl.DataFrame):

        insert_mysql = f'''
                INSERT INTO {self.table_name} (document_id, paciente_id, paciente_dataNascimento,
                    paciente_idade, paciente_enumSexoBiologico, paciente_racaCor_valor, paciente_endereco_uf,
                    vacina_fabricante_nome, vacina_lote, vacina_dataAplicacao, data_importacao_rnds)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            '''

        logging.info(f"ingest process on MySql {self.table_name} table started")

        with self.cursor as db_conn:
            tuple_values = [(lista) for lista in data_pl.iter_rows()]
            db_conn.executemany(insert_mysql, tuple_values)
            self.cnx.commit()
            self.cnx.close()

        logging.info(f"The data has been successfully inserted into the {self.table_name} table!")
