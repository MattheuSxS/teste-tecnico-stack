import logging
import polars as pl
import re
from datetime import datetime

logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


class GCStorage:
    def __init__(self, storage_client, bucket_name):
        self.client = storage_client
        self.bucket_name = bucket_name


    def get_bucket(self):
        return self.client.get_bucket(self.bucket_name)


    def upload_to_bucket(self, blob_name:str, file_path:str, bucket_name:str):
        '''
        Upload file to a bucket
        : blob_name  (str) - object name
        : file_path (str)
        : bucket_name (str)
        '''
        full_path = '{}{}'.format(file_path, blob_name)
        try:
            logging.info(f'Sending the file to bucket: {bucket_name}')
            blob = self.get_bucket().blob(blob_name)
            blob.upload_from_filename(full_path)
            logging.info(f'file {blob_name} successfully uploaded to bucket {bucket_name}')
        except Exception as e:
            raise 'Insert error failed: {}'.format(e)

        return blob
