import requests
from requests.auth import HTTPBasicAuth
import logging
import os
import polars as pl
import json
from scripts.schema import column_schema
from datetime import datetime
import re

logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


username: str = os.environ.get('API_USERNAME')
password: str = os.environ.get('API_PASSWORD')
airflow_place: int = int(os.environ.get('AIRFLOW_PLACE', 2))


if airflow_place == 1:
    file_path = '/home/airflow/gcsfuse/data/'

if airflow_place == 2:
    file_path = '/opt/airflow/dags/temp_data/'


def _build_url() -> tuple:

    logging.info(f"Building URL")
    auth = HTTPBasicAuth(username, password)

    headers = {
        "Content-Type": "application/json"
    }

    body = {
        "size": 10_000
    }

    base_url = "https://imunizacao-es.saude.gov.br/_search?scroll=1m"

    logging.info(f"URL Constructed")

    return auth, headers, base_url, body


def _get_data() -> dict:

    auth, headers, base_url, body = _build_url()

    logging.info(f"Making a request to the https://imunizacao-es.saude.gov.br API")

    response = requests.post(url=base_url, headers=headers, data=json.dumps(body), auth=auth)
    assert response.status_code == 200, \
        f'problem in the request! Status: {response.status_code}'
    logging.info(f"Request made successfully! Status: {response.status_code}")

    return response.json()


def _create_dataframe() -> pl.DataFrame:

    data_dict:dict = _get_data()

    logging.info("Dataframe creation in progress")

    df_default = pl.DataFrame()
    schema:dict = column_schema()

    for _ in range(len(data_dict['hits']['hits'])):
        data_append = pl.DataFrame(data_dict['hits']['hits'][_]['_source'], schema=schema)
        df_default = pl.concat([df_default, data_append], how="vertical")

    logging.info(f"Dataframe successfully created!")

    return df_default


def _select_columns() -> pl.DataFrame:
    df_data = _create_dataframe()

    assert df_data.is_empty() != True, \
        f'Error -> Dataframe came from _create_dataframe() empty!'

    logging.info(f"Selecting the columns")
    df_data = df_data.select(pl.col([
        'document_id',
        'paciente_id',
        'paciente_dataNascimento',
        'paciente_idade',
        'paciente_enumSexoBiologico',
        'paciente_racaCor_valor',
        'paciente_endereco_uf',
        'vacina_fabricante_nome',
        "vacina_lote",
        "vacina_dataAplicacao",
        "data_importacao_rnds",
        ]))
    logging.info(f"Columns selected successfully!")

    return df_data


def _data_cleaning() -> pl.DataFrame:

    df_clean = _select_columns()

    assert df_clean.is_empty() != True, \
        f'Error -> Dataframe came from _select_columns() empty!'

    logging.info('DataFrame in cleanup step')
    df_clean = df_clean.filter(df_clean.is_unique())
    df_clean = df_clean.filter(pl.col(['document_id']).is_unique())
    df_clean = df_clean.filter(~pl.all(pl.all().is_null()))
    df_clean = df_clean.with_columns(pl.when(
        pl.col('vacina_dataAplicacao').is_null())
        .then(pl.col('data_importacao_rnds'))
        .otherwise(pl.col('vacina_dataAplicacao')).alias('vacina_dataAplicacao')
    )
    logging.info('Cleaning step completed successfully!')

    return df_clean


def _data_transformation() -> pl.DataFrame:

    df_transformation = _data_cleaning()

    assert df_transformation.is_empty() != True, \
        f'Error -> Dataframe came from _data_cleaning() empty!'

    logging.info('Dataframe in transform step')

    date_format = '%Y-%m-%d'
    # Exemple --> "2023-03-16T23:00:00.936Z"
    date_time_format = '%Y-%m-%dT%H:%M:%S.%fZ'

    df_transformation = df_transformation.with_columns(
        pl.col('paciente_dataNascimento')
        .str.strptime(pl.Date, fmt=date_format)
        .cast(pl.Date))

    df_transformation = df_transformation.with_columns(
        pl.col('vacina_dataAplicacao')
        .str.strptime(pl.Datetime, fmt=date_time_format)
        .cast(pl.Datetime))

    df_transformation = df_transformation.with_columns(
        pl.col('data_importacao_rnds')
        .str.strptime(pl.Datetime, fmt=date_time_format)
        .cast(pl.Datetime))

    df_transformation = df_transformation.with_columns(
        pl.when(pl.col('paciente_racaCor_valor') == '1').then('Branca')
        .when(pl.col('paciente_racaCor_valor') == '2').then('Preto')
        .when(pl.col('paciente_racaCor_valor') == '3').then('Parda')
        .when(pl.col('paciente_racaCor_valor') == '4').then('Amarela')
        .otherwise('Sem Informações').alias('paciente_racaCor_valor')
        )

    df_transformation = df_transformation.with_columns(
        pl.when(pl.col('paciente_enumSexoBiologico') == 'M').then('Masculino')
        .when(pl.col('paciente_enumSexoBiologico') == 'F').then('Feminino')
        .otherwise('Sem Informações').alias('paciente_enumSexoBiologico')
        )

    logging.info('Transformation step completed successfully')

    return df_transformation


def get_data() -> pl.DataFrame:
    return _data_transformation()


def create_parquet_file(data: pl.DataFrame) -> str:

    logging.info('Process to write the .parquet file started')
    _datetime = re.subn(r'[^0-9]', '', str(datetime.now()))[0]
    file_name = 'SUS-COVID-19-{}.parquet'.format(_datetime)
    full_path = '{}{}'.format(file_path, file_name)

    data.write_parquet(
        file=full_path,
        compression='zstd',
        compression_level=5
        )

    logging.info(f'File {file_name} generated successfully!')

    return full_path


def remove_all_files() -> None:

    logging.info('File(s) deletion step started')
    files = os.listdir(file_path)

    for filename in files:
        os.remove(f'{file_path}{filename}')
        logging.info(f"File {filename} Deleted successfully!")
