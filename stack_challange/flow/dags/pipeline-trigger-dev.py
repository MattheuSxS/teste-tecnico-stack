from datetime import timedelta, datetime, timezone
from airflow.decorators import dag, task
import logging
from scripts.modulos import get_data, create_parquet_file, remove_all_files
import os
import polars as pl
from scripts.gcp_bucket import GCStorage
from scripts.gcp_bigquery import BigQuery
from scripts.gcp_bigtable import BigTable
from scripts.gcp_mysql import SqlCloudMySql
from google.cloud import storage
from google.cloud import bigquery
from google.cloud import bigtable


logging.basicConfig(
    format=("%(asctime)s | %(levelname)s | File_name ~> %(module)s.py "
            "| Function ~> %(funcName)s | Line ~> %(lineno)d  ~~>  %(message)s"),
    level=logging.DEBUG
    )


timezone = timezone(timedelta(hours=-3))
default_args = {
    'dag_id': 'stack_challange',
    'start_date': datetime(2023, 3, 5, tzinfo=timezone),
    'retries': 3,
    'retry_delay': timedelta(minutes=60),
    'description': "This DAG triggers the pipeline job",
    'project_id': 'Data_Engineering',
    'tags': ["stack_challange"]
}

@dag(default_args=default_args, schedule_interval='30 6 5 1-12 *')

def exec_pipeline_dag():
    '''
        Google Cloud Platform information
    '''
    project_id      = os.environ.get('GCP_PROJECT_ID')
    bucket_id       = os.environ.get('GCP_BUCKET_ID')
    dataset_id      = os.environ.get('GCP_BIGQUERY_DATASET_ID')
    table_id        = os.environ.get('GCP_BIGQUERY_TABLE_ID')
    instance_id     = os.environ.get('GCP_BIGTABLE_INSTANCE_ID')
    bt_table_id     = os.environ.get('GCP_BIGTABLE_TABLE_ID')


    @task()
    def get_dataframe() -> str:
        gold_data = get_data()
        path_parquet = create_parquet_file(gold_data)

        print(path_parquet)
        logging.info(path_parquet)

        return path_parquet


    @task()
    def update_bucket_gcp(**context):
        full_path = context['ti'].xcom_pull(task_ids='get_dataframe')
        fila_name = full_path.split("/")[-1]
        file_path = full_path.replace(fila_name, "")

        storage_client = storage.Client()
        gcs = GCStorage(storage_client=storage_client, bucket_name=bucket_id)

        file_list = os.listdir(file_path)
        for filename in file_list:
            gcs.upload_to_bucket(blob_name=filename, bucket_name=bucket_id, file_path=file_path)


    @task()
    def bigquery_insert(**context):
        file_path = context['ti'].xcom_pull(task_ids='get_dataframe')
        gold_data = pl.read_parquet(file_path)

        bigquery_client = bigquery.Client()
        bq = BigQuery(bigquery_client=bigquery_client, project_id=project_id,
                    dataset_id=dataset_id, table_id=table_id)

        bq.insert_bigquery(gold_data)


    @task()
    def bigtable_insert(**context):
        file_path = context['ti'].xcom_pull(task_ids='get_dataframe')
        gold_data = pl.read_parquet(file_path)

        bigtable_client = bigtable.Client(project=project_id)
        bt = BigTable(bigtable_client=bigtable_client, instance_id=instance_id,
                    bt_table_id=bt_table_id)

        bt.insert_bigtable(gold_data)


    @task()
    def cloud_mysql_insert(**context):
        file_path = context['ti'].xcom_pull(task_ids='get_dataframe')
        gold_data = pl.read_parquet(file_path)

        mysql_client = SqlCloudMySql()
        mysql_client.create_table()
        mysql_client.insert_mysql(gold_data)

    @task()
    def remove_all_temp_files():
        remove_all_files()


    @task()
    def successfully():
        return 'application finished and all data layers loaded successfully!'


    get_dataframe() >> [update_bucket_gcp(),
                        bigquery_insert(),
                        bigtable_insert(),
                        cloud_mysql_insert()] >> remove_all_temp_files() >> successfully()


exec_pipeline_dag()