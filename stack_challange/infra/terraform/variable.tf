variable "project" {
    type    = string
    default = "gcp-data-pipeline-stack"
}

variable "region" {
    type    = string
    default = "us-east1"
}

variable "zone" {
    type    = string
    default = "us-east1-b"
}

#   ****************************************************    #
#                           BigQuery                        #
#   ****************************************************    #
variable "bigquery_dataset_name" {
    type    = string
    default = "product_analytical"
}

variable "bigquery_table_name" {
    type    = string
    default = "sus_analytical"
}

#   ****************************************************    #
#                         Cloud  Bucker                     #
#   ****************************************************    #

variable "cloud_storage_name" {
    type    = string
    default = "bkt-filesystem-stack"
}

variable "stotage_class_standard" {
    type    = string
    default = "STANDARD"
}

variable "stotage_class_nearline" {
    type    = string
    default = "NEARLINE"
}

variable "stotage_class_coldline" {
    type    = string
    default = "COLDLINE"
}

variable "stotage_class_archive" {
    type    = string
    default = "ARCHIVE"
}

#   ****************************************************    #
#                         Cloud Sql MySql                   #
#   ****************************************************    #

variable "mysql_instance_name" {
    type    = string
    default = "stack-instance-mysql"
}

variable "mysql_database_name" {
    type    = string
    default = "stack-database"
}

variable "sql_mysql_username" {
    type        = string
    description = "(optional) username id"
}

variable "sql_mysql_password" {
    type        = string
    description = "(optional) password"
}

variable "sql_mysql_table_name" {
    type        = string
    default     = "analytica_stack"
    description = "Nome da tabela"
}

variable "sql_mysql_ips" {
    type        = list(string)
    description = "(optional) password"
}

#   ****************************************************    #
#                         Cloud BigTable                    #
#   ****************************************************    #

variable "bigtable_instance_name" {
    type    = string
    default = "stack-instance-bigtable"
}

variable "bigtable_table_name" {
    type    = string
    default = "analytical-stack"
}

variable "bigtable_cluster_name" {
    type    = string
    default = "bigtable-instance-stack"
}

#   ****************************************************    #
#                         cloud Composer                    #
#   ****************************************************    #

variable "cloud_composer_name" {
    type    = string
    default = "airflow-stack"
}