resource "google_composer_environment" "airflow-stack-academy" {
    project = var.project
    name    = var.cloud_composer_name
    region  = var.region

    config {

        node_count = 3

        node_config {
            disk_size_gb    = 100
            zone            = var.zone
            machine_type    = "n1-standard-2"
        }

        software_config {
            image_version   = "composer-1.20.10-airflow-2.4.3"
            python_version  = "3"
            pypi_packages   = {
                    requests                = "==2.28.2",
                    polars                  = "==0.16.17",
                    mysql-connector-python  = "==8.0.32"
            }
            env_variables   = {
                API_USERNAME                ="imunizacao_public",
                API_PASSWORD                ="qlto5t&7r_@+#Tlstigi",
                GCP_PROJECT_ID              = var.project,
                GCP_PROJECT_REGION          = var.region,
                GCP_BIGQUERY_DATASET_ID     = var.bigquery_dataset_name,
                GCP_BIGQUERY_TABLE_ID       = var.bigquery_table_name,
                GCP_BUCKET_ID               = var.cloud_storage_name,
                GCP_BIGTABLE_INSTANCE_ID    = var.bigtable_instance_name,
                GCP_BIGTABLE_TABLE_ID       = var.bigtable_table_name,
                GCP_INSTANCE_MYSQL          = var.mysql_instance_name,
                GCP_DATABASE_MYSQL          = var.mysql_database_name,
                GCP_TABLE_MYSQL             = var.sql_mysql_table_name,
                # GCP_INSTANCE_PUBLIC_IP    = < pegar e inserido manualmente depois de criar a instancia MYSQL via TERRAFORM > >
                TF_VAR_sql_mysql_username   = "matheus",
                TF_VAR_sql_mysql_password   = "87654321a",
                AIRFLOW_PLACE               = 1
            }
        }

        web_server_config {
            machine_type = "composer-n1-webserver-2"
        }
    }

    lifecycle {
        prevent_destroy = false
    }

}
