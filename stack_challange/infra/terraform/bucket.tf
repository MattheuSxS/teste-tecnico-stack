resource "google_storage_bucket" "bkt-filesystem-stack" {
    name                            = var.cloud_storage_name
    project                         = var.project
    location                        = var.region
    storage_class                   = var.stotage_class_standard
    force_destroy                   = true
    uniform_bucket_level_access     = true

    # Habilitar versionamento
    versioning {
        enabled = true
    }

    # regra ciclo de vida
    lifecycle_rule {
        condition {
            age = 90
        }
        action {
            type = "SetStorageClass"
            storage_class = var.stotage_class_nearline
        }
    }

    lifecycle_rule {
        condition {
            age = 150
        }
        action {
            type = "SetStorageClass"
            storage_class = var.stotage_class_coldline
        }
    }

    lifecycle_rule {
        condition {
            age = 180
        }
        action {
            type = "SetStorageClass"
            storage_class = var.stotage_class_archive
        }
    }

    lifecycle_rule {
        condition {
            num_newer_versions = 3
        }
        action {
            type = "Delete"
        }
    }
}


resource "google_storage_bucket_object" "my_dags" {
    depends_on  = [google_composer_environment.airflow-stack-academy]

    for_each    = fileset("../../flow/dags/", "**.py")
    name        = "dags/${each.key}"
    bucket      = replace(replace(google_composer_environment.airflow-stack-academy.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
    source      = "../../flow/dags/${each.value}"
}


resource "google_storage_bucket_object" "modules_files" {
    depends_on  = [google_composer_environment.airflow-stack-academy]

    for_each    = fileset("../../flow/dags/scripts/", "**.py")
    name        = "dags/scripts/${each.value}"
    bucket      = replace(replace(google_composer_environment.airflow-stack-academy.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
    source      = "../../flow/dags/scripts/${each.value}"
}


resource "google_storage_bucket_object" "json_key" {
    depends_on  = [google_composer_environment.airflow-stack-academy]

    for_each    = fileset("../../flow/dags/scripts/", "**.json")
    name        = "dags/scripts/${each.value}"
    bucket      = replace(replace(google_composer_environment.airflow-stack-academy.config[0].dag_gcs_prefix, "gs://", ""), "/dags", "")
    source      = "../../flow/dags/scripts/${each.value}"
}