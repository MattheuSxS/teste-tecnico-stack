resource "google_bigquery_dataset" "product_analytical" {
    dataset_id                 = var.bigquery_dataset_name
    friendly_name              = var.bigquery_dataset_name
    description                = "Analytical dataset - test tecnico stack"
    project                    = var.project
    location                   = var.region
    delete_contents_on_destroy = false

    labels = {
        env = "default"
        }
}

resource "google_bigquery_table" "sus_analytical" {
    project             = var.project
    dataset_id          = var.bigquery_dataset_name
    table_id            = var.bigquery_table_name
    deletion_protection = false
    depends_on = [google_bigquery_dataset.product_analytical]

    time_partitioning {
        type    = "DAY"
        field   = "vacina_dataAplicacao"
    }

    labels = {
        "created_by": "terraform",
        "layer": "good"
    }

    clustering = [
        "vacina_fabricante_nome",
        "vacina_lote"
    ]

    schema = file("${path.module}/schemas/sus_analytical.json")

}