resource "google_bigtable_instance" "stack-instance-bigtable" {
  name                = var.bigtable_instance_name
  project             = var.project
  deletion_protection = false

  cluster {
    cluster_id   = var.bigtable_cluster_name
    zone         = var.zone
    num_nodes    = 3
    storage_type = "SSD"
  }

  lifecycle {
    prevent_destroy = false
  }
}

resource "google_bigtable_table" "analytical-stack" {
  name          = var.bigtable_table_name
  instance_name = google_bigtable_instance.stack-instance-bigtable.name
  depends_on    = [google_bigtable_instance.stack-instance-bigtable]

  lifecycle {
    prevent_destroy = false
  }

  column_family {
    family = "stack-family"
    }
}