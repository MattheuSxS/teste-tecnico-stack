locals {
  onprem = var.sql_mysql_ips
}

resource "google_sql_database_instance" "stack-instance-mysq" {
    name                = var.mysql_instance_name
    project             = var.project
    region              = var.region
    root_password       = var.sql_mysql_password
    database_version    = "MYSQL_8_0"
    deletion_protection = "false"

  settings {
    tier = "db-f1-micro"

    ip_configuration {
        dynamic "authorized_networks" {
            for_each = local.onprem
            iterator = onprem

            content {
            name  = "onprem-${onprem.key}"
            value = onprem.value
            }
            }
        }
    }

    lifecycle {
        prevent_destroy = false
    }
}


resource "google_sql_database" "stack-database" {
    name        = var.mysql_database_name
    project     = var.project
    instance    = google_sql_database_instance.stack-instance-mysq.name
    depends_on  = [google_sql_database_instance.stack-instance-mysq]

    lifecycle {
        prevent_destroy = false
    }
}

resource "google_sql_user" "users" {
    name        = var.sql_mysql_username
    project     = var.project
    password    = var.sql_mysql_password
    instance    = google_sql_database_instance.stack-instance-mysq.name
    type        = "BUILT_IN"
    depends_on  = [google_sql_database_instance.stack-instance-mysq]
}