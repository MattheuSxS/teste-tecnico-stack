terraform {
    required_providers {
        google = {
            source = "hashicorp/google"
            version = "4.57.0"
            }
        }
    backend "gcs" {
        bucket = "bkt-test-stack-dev-tfstate"
        prefix = "terraform/state"
    }
}

provider "google" {
    project = var.project
    region  = var.region
}



