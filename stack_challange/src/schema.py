import polars as pl
from loguru import logger
from collections import OrderedDict

def column_schema() -> dict:
    schema={
        'vacina_grupoAtendimento_nome': pl.Utf8,
        'vacina_codigo': pl.Utf8,
        'paciente_dataNascimento': pl.Date,
        'ds_condicao_maternal': pl.Utf8,
        'paciente_endereco_nmPais': pl.Utf8,
        'paciente_racaCor_valor': pl.Utf8,
        'sistema_origem': pl.Utf8,
        'paciente_id': pl.Utf8,
        'paciente_endereco_uf': pl.Utf8,
        'paciente_idade': pl.Int32,
        'paciente_endereco_cep': pl.Utf8,
        'estabelecimento_valor': pl.Utf8,
        'estabelecimento_municipio_codigo': pl.Utf8,
        'data_importacao_datalake': pl.Utf8,
        'paciente_enumSexoBiologico': pl.Utf8,
        'estabelecimento_municipio_nome': pl.Utf8,
        'vacina_grupoAtendimento_codigo': pl.Utf8,
        'vacina_descricao_dose': pl.Utf8,
        'paciente_endereco_nmMunicipio': pl.Utf8,
        'vacina_categoria_nome': pl.Utf8,
        'vacina_fabricante_nome': pl.Utf8,
        'vacina_categoria_codigo': pl.Utf8,
        'dt_deleted': pl.Utf8,
        'paciente_nacionalidade_enumNacionalidade': pl.Utf8,
        'vacina_numDose': pl.Utf8,
        'status': pl.Utf8,
        'document_id': pl.Utf8,
        'vacina_lote': pl.Utf8,
        'id_sistema_origem': pl.Utf8,
        '@timestamp': pl.Utf8,
        'estalecimento_noFantasia': pl.Utf8,
        '@version': pl.Utf8,
        'paciente_racaCor_codigo': pl.Utf8,
        'estabelecimento_razaoSocial': pl.Utf8,
        'vacina_nome': pl.Utf8,
        'estabelecimento_uf': pl.Utf8,
        'data_importacao_rnds': pl.Utf8,
        'vacina_dataAplicacao': pl.Utf8,
        'co_condicao_maternal': pl.Int32,
        'paciente_endereco_coPais': pl.Utf8,
        'vacina_fabricante_referencia': pl.Utf8,
        'paciente_endereco_coIbgeMunicipio': pl.Utf8,
    }

    return schema
