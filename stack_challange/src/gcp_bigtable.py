import polars as pl
from loguru import logger
import time


class BigTable:
    def __init__(self, bigtable_client, instance_id, bt_table_id):
        self.client = bigtable_client
        self.instance_id = instance_id
        self.bt_table_id = bt_table_id


    def insert_bigtable(self, data: pl.DataFrame) -> None:

        logger.info(f"Started ingesting data into {self.instance_id}.{self.bt_table_id} table")

        self.instance = self.client.instance(self.instance_id)
        self.table = self.instance.table(self.bt_table_id)

        data_dict = data.to_dict()
        column_family_id = 'stack-family'

        try:
            row_key = f"SUS_{int(time.time())}"
            row = self.table.row(row_key)
            for col_family_name, col_family_data in data_dict.items():
                row.set_cell(column_family_id, col_family_name, str(col_family_data).encode('utf-8').strip())
                row.commit()

            logger.info(f"Successfully ingested data into the {self.instance_id}.{self.bt_table_id} table")
        except Exception as e:
            raise 'Insert error failed: {}'.format(e)