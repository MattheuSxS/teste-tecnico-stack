from loguru import logger
from modulos import data_transformation, create_parquet_file, remove_all_files
import os
from gcp_bucket import GCStorage
from gcp_bigquery import BigQuery
from gcp_bigtable import BigTable
from gcp_mysql import SqlCloudMySql
from google.cloud import storage
from google.cloud import bigquery
from google.cloud import bigtable


__app_name__ = 'stack_challange'
file_path = '../datasets/'

'''
    Google Cloud Platform information
'''
project_id  = os.environ.get('GCP_PROJECT_ID')
bucket_id   = os.environ.get('GCP_BUCKET_ID')
dataset_id  = os.environ.get('GCP_BIGQUERY_DATASET_ID')
table_id    = os.environ.get('GCP_BIGQUERY_TABLE_ID')
instance_id = os.environ.get('GCP_BIGTABLE_INSTANCE_ID')
bt_table_id = os.environ.get('GCP_BIGTABLE_TABLE_ID')

def main() -> None:

    logger.info('initialized application')
    gold_data = data_transformation()

    '''
        filesystem local
    '''
    create_parquet_file(gold_data)

    '''
        Cloud Storage -> filesytem
    '''
    file_list = os.listdir(file_path)
    storage_client = storage.Client()
    gcs = GCStorage(storage_client=storage_client, bucket_name=bucket_id)

    for filename in file_list:
        gcs.upload_to_bucket(blob_name=filename, bucket_name=bucket_id, file_path=file_path)

    # '''
    #     BigQuery --> analítico
    # '''
    bigquery_client = bigquery.Client()
    bq = BigQuery(bigquery_client=bigquery_client, project_id=project_id,
                   dataset_id=dataset_id, table_id=table_id)

    bq.insert_bigquery(gold_data)

    '''
        BigTable NoSql
    '''
    bigtable_client = bigtable.Client(project=project_id)
    bt = BigTable(bigtable_client=bigtable_client, instance_id=instance_id,
                  bt_table_id=bt_table_id)

    bt.insert_bigtable(gold_data)

    '''
        Database MySql
    '''
    mysql_client = SqlCloudMySql()
    mysql_client.create_table()
    mysql_client.insert_mysql(gold_data)

    logger.info('removing all files from filesystem')
    remove_all_files()
    logger.info('application finished and all data layers loaded successfully!')


if __name__ == "__main__":
    main()