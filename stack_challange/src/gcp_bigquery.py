import polars as pl
from loguru import logger


class BigQuery:
    def __init__(self, bigquery_client, project_id, dataset_id, table_id):
        self.client = bigquery_client
        self.project_id = project_id
        self.dataset_id = dataset_id
        self.table_id = table_id


    def insert_bigquery(self, data: pl.DataFrame) -> None:

        logger.info(f"Started ingesting data into {self.dataset_id}.{self.table_id} table")

        self.data_dict = data.to_dict()
        self.data_list = [{k: v[i] for k, v in self.data_dict.items()} for i in range(len(data))]
        self.path_table = '{}.{}.{}'.format(self.project_id, self.dataset_id, self.table_id)

        try:
            table = self.client.get_table(self.path_table)
            insert_job = self.client.insert_rows(table, self.data_list)
            logger.info(f"Success! {len(self.data_list)} rows were inserted.")
        except Exception as e:
            raise 'Insert error failed: {}'.format(e)
