from setuptools import setup, find_packages

import version


setup(
    name='Stack-Challenge',
    version=version.VERSION,

    description='Stack Challenge',

    author='Matheus Dos S. Silva',
    author_email='mattheusxs@gmail.com',

    packages=find_packages(exclude=['venv', 'dist', 'docs', 'tests']),
    py_modules=[
        'version',
        'modulos',
        'schema',
        'gcp_bigquery',
        'gcp_bigtable',
        'gcp_bucket',
        'gcp_mysql'
    ],
    install_requires=[
        'requests==2.28.2',
        'polars==0.16.13',
        'loguru==0.6.0',
        'google-cloud-bigquery==3.7.0',
        'google-cloud-bigtable==2.17.0',
        'google-cloud-storage==2.7.0',
        'google-cloud-core==2.3.2',
        'google-api-core==2.11.0',
        'mysql-connector-python==8.0.32',
        'Flask',
        'pytest',
        'coverage',
        'pytest-cov',
        'pytest-html',
        'mock',
        'pylint',
        'autopep8',
        'unidecode'
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'Operating System :: POSIX',
        'Programming Language :: Python :: 3.10.9',
        'Topic :: Scientific/Engineering :: Information Analysis'
    ],
    entry_points='''
        [console_scripts]
        spb_etl=main:cli
    '''
)
