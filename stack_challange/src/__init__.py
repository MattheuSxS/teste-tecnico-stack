import os

def cloud_auth():
    try:
        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = r'gcp-data-pipeline-stack-36a9636b1b74.json'
    except Exception as e:
        raise f'problem reading .json file: {e}'

cloud_auth()